import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style: '',
            color: '',
            picture: '',
            locations: []
        };
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations
        console.log(data)

        const hatURL = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            const cleared = {
                fabric: '',
                style: '',
                color: '',
                picture: '',
                locations: [],
            };
            this.setState(cleared);
        }

    }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({fabric: value})
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({style: value})
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();

          this.setState({locations: data.locations})
        }
      }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={this.handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required name="fabric" id="fabric" className="form-control" />
                        <label htmlFor="fabric">fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleStyleNameChange} value={this.state.styleName} placeholder="Style Name" required name="style_name" id="style_name" className="form-control" />
                        <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleColorChange} vlaue={this.state.color} placeholder="Color" required name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="mb-3">
                        <input onChange={this.handleUrlChange} value={this.state.url} placeholder="Picture" type="url" name="picture_url" id="picture_url" className="form-control"></input>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                        <option value="">Choose a location</option>
                        {this.state.locations.map(location => {
                            return (
                                <option key={location.closet_name} value={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        )
    }
}

export default HatForm;

