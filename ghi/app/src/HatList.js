import React, { useState } from "react";

function HatList(props) {
    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatList;
