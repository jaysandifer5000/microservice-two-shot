from shoes_rest.models import BinVO
import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.


def get_bins():
    response = requests.get("http:://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for bin in content("bins"):
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={"closet_name": bin["closet_name"], }
        )
# ToDO something wrong with lines 21 and 22 no idea what though


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            get_bins()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
