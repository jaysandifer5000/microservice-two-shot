from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "model_name"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "color", "picture_url", "manufacturer", "id"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "color", "picture_url", "manufacturer", "id"]

    # def get_extra_data(self, o):
    #     return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            bin_id = content["bin"]
            bin = BinVO.objects.get(id=bin_id)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, pk):

    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe Does not exist"})
