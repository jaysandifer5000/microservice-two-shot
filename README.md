# Wardrobify

Team:

* Jonathan Sandifer - Hats
* Shane Siebler - Shoes

## Design
Have a website that allows a user to add their hats and shoes to an online Wardrobe, 
to quickly view and organize according to their wishes.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice
The Hat model conveys the style, fabric, color, and a picture of the hat. 
With a tie in of the location of the hat inside the Wardrobe.

