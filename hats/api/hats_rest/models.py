from tkinter import CASCADE
from django.db import models
from django.urls import reverse

# Create your models here.
# Hats have to have a  fabric, style name, color, a picture url, and location in the wardrobe 
#of where it exists.

# This is for the hat, wardrobe location
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_section = models.CharField(max_length=100)
    drawer_section = models.PositiveSmallIntegerField(null=True)
    shelf_section = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_section


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True,
    )

    # Dont remeber why I have to add this I just do i guess.
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
    
    def __str__(self):
        return self.style_name
    
    class Meta:
        ordering = ("style_name", "color", "location") #This should be the order of my Hats