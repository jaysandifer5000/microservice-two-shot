from django.urls import path
from django.contrib import admin
from .views import (
    api_list_hats,
    api_show_hats,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path("hats/", api_list_hats, name="list_hats" ),
    path("hats/<int:pk>/", api_show_hats, name="show_hats" ),
]
