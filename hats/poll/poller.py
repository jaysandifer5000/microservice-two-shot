import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
# Is that something, from hats_rest.models import Location? Or is it something I still have to create
from hats_rest.models import LocationVO

def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for location in content["locations"]:
        Location.objects.update_or_create(
            import_href=location["href"],
            defaults={
                "closet_section": location["closet_section"],
                "drawer_section": location["drawer_section"],
                "shelf_section": location["shelf_section"],

            },

        )
            # Write your polling logic, here
def poll():
    while True:
        print("Hat is polling for data")
        try:
            get_locations()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
